<?php

namespace Drupal\geoipapi;

use Drupal\Core\StreamWrapper\PrivateStream;
use GeoIp2\Database\Reader;
use Psr\Log\LoggerInterface;
use Drupal\Core\File\FileSystem;

class Resolver {

  /**
   * The database reader.
   *
   * @var \GeoIp2\Database\Reader
   */
  protected $reader;

  /**
   * Local path to the geoip database.
   *
   * @var string
   */
  protected $db_dir = "private://geoip2/";

  /**
   * The logger.
   *
   * @var LoggerInterface
   */
  protected $logger;

  /**
   * The file system.
   *
   * @var \Drupal\Core\File\FileSystem
   */
  protected $fileSystem;

  /**
   * Path to the remote MaxMind database.
   *
   * @var string
   */
  const GEOLITECITY_PATH = "http://geolite.maxmind.com/download/geoip/database/GeoLite2-City.mmdb.gz";

  /**
   * Path to the remote MaxMind database.
   *
   * @var string
   */
  const GEOLITECOUNTRY_PATH = "http://geolite.maxmind.com/download/geoip/database/GeoLite2-Country.mmdb.gz";

  /**
   * Path to the database
   * 
   * @var string
   * */
  protected $db_path = '';

  /**
   * Path to the local geolite database.
   *
   * @return string
   */
  protected function databasePath() {
    if (!file_exists($this->db_dir)) {
      $this->fileSystem->mkdir($this->db_dir, FILE_CREATE_DIRECTORY);
    }
    return drupal_realpath($this->db_dir) . "/GeoLite2-City.mmdb";
  }

  /**
   * Gets an instance of Resolver
   */
  public function __construct(LoggerInterface $logger, FileSystem $fileSystem) {
    $this->logger = $logger;
    $this->fileSystem = $fileSystem;
    $this->db_path = $this->databasePath();
    if (!file_exists($this->db_path)) {
      $this->syncDatabase();
    }
  }

  function uncompress($srcName, $dstName) {
    $sfp = gzopen($srcName, "rb");
    $fp = fopen($dstName, "w");

    while (!gzeof($sfp)) {
      $string = gzread($sfp, 4096);
      fwrite($fp, $string, strlen($string));
    }
    gzclose($sfp);
    fclose($fp);
  }

  /**
   * Make sure that the database is downloaded and up
   * to date.
   */
  public function syncDatabase() {
    if (empty(PrivateStream::basePath())) {
      $this->logger->log(\Psr\Log\LogLevel::DEBUG, "Setup a private:// directory to download the GeoIp database.");
      return;
    }
    // Check if the city database exists or if it is older than 8 days.
    if (!file_exists($this->db_path) || time() - filemtime($this->db_path) > (8 * 3600 * 24)) {
      $filename = basename(self::GEOLITECITY_PATH);
      $dir = "temporary://geoipapi/downloads/";
      file_prepare_directory($dir, FILE_CREATE_DIRECTORY);
      $tempfile = $this->fileSystem->realpath($dir . "/$filename");
      $tempfile_extracted = str_replace('.gz', '', $tempfile);
      file_put_contents($tempfile, fopen(self::GEOLITECITY_PATH, 'r'));

      // decompress from gz
      $this->uncompress($tempfile, $tempfile_extracted);

      // Look for the mmdb file.
      file_prepare_directory($dir, dirname($this->db_path));
      rename($tempfile_extracted, $this->db_path);
      file_unmanaged_delete_recursive($dir);
    }
  }

  /**
   * Ensure that the reader is loaded and working.
   */
  protected function ensureReader() {
    if (empty($reader)) {
      $this->reader = new Reader($this->db_path);
    }
  }

  /**
   * Geolocate an IP address.
   *
   * @param string $ip
   *
   * @return \GeoIp2\Model\City|FALSE
   */
  function resolveIP($ip) {
    $this->ensureReader();
    /** @var \GeoIp2\Model\City $record */
    $record = NULL;
    try {
      $record = $this->reader->city($ip);
    }
    catch (\GeoIp2\Exception\AddressNotFoundException $ex) {
      return FALSE;
    }
    return $record;
  }
}
